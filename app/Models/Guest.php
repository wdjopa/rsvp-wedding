<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    use HasFactory;

    protected $casts = [
        'meta' => 'array',
    ];

    public function table()
    {
        return $this->belongsTo(Table::class);
    }
}
