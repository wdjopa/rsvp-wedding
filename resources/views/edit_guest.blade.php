@extends('simple_layout')
@section('title', "Ajout d'un invité")
@section('content')
    <form method="POST" action="{{ route('guests.update', $guest) }}">
        @csrf
        @method('PUT')
        <h3>
            Edit a guest
        </h3>
        @include('guest_form', ['guest' => $guest])
        <button class="bg-green-700 px-4 py-2 rounded-md text-white mb-4">
            Enregistrer
        </button>
    </form>
    <form method="POST" action="{{ route('guests.delete', $guest) }}">
        @csrf
        @method('DELETE')
        <button class="bg-red-700 px-4 py-2 rounded-md text-white ">
            Supprimer
        </button>
    </form>
@endsection
