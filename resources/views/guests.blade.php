<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
</head>


<body>
    <h1 class="text-3xl font-bold underline text-center my-6">
        Liste des invités
        <span class="guest_counter">
            ({{ $guests->reduce(function ($prev, $item) {
                return $prev + ($item->is_couple ? 2 : 1);
            }, 0) }})
        </span>
    </h1>


    @auth
        <div class="container text-center">
            <a href="{{ route('guests.create') }}" class="btn btn-primary my-4">
                Ajouter un invité
            </a>
        </div>
    @endauth
    <div class="container">
        <table class="table-auto mx-auto py-4 " id="guest">
            <thead>
                <tr class="bg-green-400 text-white rounded-md overflow-hidden">
                    <th>ID</th>
                    <th>Prénom</th>
                    <th>Nom</th>
                    <th>Table</th>
                    <th>Couple ?</th>
                    <th>De</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($guests as $guest)
                    <tr>
                        <td>
                            <a href="{{ route('guests.show', $guest) }}">
                                {{ $guest->id }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('guests.show', $guest) }}">
                                {{ $guest->first_name }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('guests.show', $guest) }}">
                                {{ $guest->last_name }}
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('table.show', ['table' => $guest->table->id]) }}">
                                {{ $guest->table->name }}
                            </a>
                        </td>
                        <td>
                            {{ $guest->is_couple ? 'OUI' : 'NON' }}
                        </td>
                        <td>
                            {{ $guest->meta['group_name'] ?? '' }}
                        </td>
                        <td>
                            <a class="btn btn-info btn-small" target="_blank"
                                href="https://wedding-ticket.wilmar.djopa.fr/single?slug={{ $guest->slug }}&name={{ ucfirst(strtolower($guest->first_name)) . ' ' . ucfirst(strtolower($guest->last_name)) }}&is_couple={{ $guest->is_couple == 0 ? 'false' : 'true' }}">Billet</a>
                            @auth
                                <a href="{{ route('guests.edit', $guest) }}" class="btn btn-small btn-warning">Modifier</a>
                                <form method="POST" action="{{ route('guests.delete', $guest) }}" class="inline"
                                    id="guest_form_{{ $guest->id }}" name="guest_form_{{ $guest->id }}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-small btn-danger text-danger" type="button"
                                        onclick="deleteGuest({{ $guest->id }})">Supprimer</button>
                                </form>

                            @endauth
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>

    <script src="//cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script>
        let table = new DataTable('#guest');
        var data = []
        table.on('draw.dt', function() {
            var filteredData = table.rows({
                filter: 'applied'
            }).data();

            // Affichage des éléments filtrés dans la console
            const data = filteredData.toArray()
            $(".guest_counter").text(`(${data.reduce((cumul, row) => {
                return cumul + (row[4] == "NON" ? 1 : 2)
            }, 0)})`)
        });

        function deleteGuest(id) {
            if (confirm("Validation de suppression le guest " + id)) {
                document.getElementById("guest_form_" + id).submit()
            }
        }

        // table.rows({
        //     "search": "applied"
        // }).every(function() {
        //     var data = this.data();
        //     console.log({
        //         data
        //     })
        // });
    </script>
</body>

</html>
