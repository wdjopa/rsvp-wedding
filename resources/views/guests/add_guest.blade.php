@extends('simple_layout')
@section('title', "Ajout d'un invité")
@section('content')
    <form method="POST" action="{{ route('guests.store') }}">
        @csrf
        <h3>
            Add a guest
        </h3>
        @include('guests.guest_form')
        <button class="bg-green-700 px-4 py-2 rounded-md text-white ">
            Enregistrer
        </button> <a href="{{ route('guests.list') }}" class="bg-slate-700 px-4 py-2 rounded-md text-white">
            Retour
        </a>
    </form>

    <script>
        $('.table_list').select2();
    </script>
@endsection
