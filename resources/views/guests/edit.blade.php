{{-- @extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('guests.update', $guest) }}">
        @csrf
        @include('guests.form', ['guest' => $guest])

        <div class="mb-4 text-right">

            <a href="{{ URL::previous() }}" class="btn btn-secondary">
                Retour
            </a>
            <button class="btn btn-success">
                Save
            </button>

        </div>
    </form>
@endsection --}}

@extends('simple_layout')
@section('title', "Modification d'un invité")
@section('content')
    <form method="POST" action="{{ route('guests.update', $guest) }}">
        @csrf
        @method('PUT')
        <h3>
            Edit a guest
        </h3>
        @include('guests.guest_form', ['guest' => $guest])

        <button class="bg-green-700 px-4 py-2 rounded-md text-white mb-3 ">
            Enregistrer
        </button>
        <a href="{{ route('guests.list') }}" class="bg-slate-700 px-4 py-2 mx-3 rounded-md text-white mb-3">
            Retour
        </a>

    </form>
    <form method="POST" action="{{ route('guests.delete', $guest) }}">
        @csrf
        @method('DELETE')
        <button class="bg-red-700 px-4 py-2 rounded-md text-white ">
            Supprimer
        </button>
    </form>
    <script>
        $('.table_list').select2();
    </script>
@endsection
