<div>
    <div class="mb-4">
        <label>Prénom</label>
        <input class="form-control" name="first_name" value="{{ old('first_name') ?? $guest->first_name }}"
            placeholder="Prénom" />
    </div>
    <div class="mb-4">
        <label>Nom</label>
        <input class="form-control" name="last_name" value="{{ old('last_name') ?? $guest->last_name }}"
            placeholder="Nom" />
    </div>

    <div class="mb-4">
        <label>Invité par </label>
        <input class="form-control" name="group_name"
            value="{{ (old('group_name') ? old('group_name') : isset($guest)) ? $guest->meta['group_name'] : '' }}"
            placeholder="Invité par qui ? " />
    </div>
    <div class="mb-4">
        <label>Billet couple ?</label>
        <select name="is_couple" class="form-control" value="{{ old('is_couple') ?? $guest->is_couple }}">
            <option @if (old('is_couple') ?? $guest->is_couple == 1) selected @endif value="1">Oui</option>
            <option @if (old('is_couple') ?? $guest->is_couple == 0) selected @endif value="0">Non</option>
        </select>
    </div>
    <div class="mb-4">
        <label>Table</label>
        <select name="table" class="form-control">
            @foreach (\App\Models\Table::all() as $table)
                <option value="{{ $table->id }}" @if (old('table') ?? $guest->table->id == $table->id) selected @endif>
                    {{ $table->name }}
                </option>
            @endforeach
        </select>
    </div>
</div>
