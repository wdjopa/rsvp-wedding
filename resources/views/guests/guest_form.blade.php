<div class="mb-5">
    <input type="text" placeholder="Prénom" name="first_name"
        class="w-full my-4 rounded-md border border-slate-400 px-4 py-3"
        value="{{ isset($guest) ? $guest->first_name : '' }}" />
    <input type="text" placeholder="Nom" name="last_name"
        class="w-full my-4 rounded-md border border-slate-400 px-4 py-3"
        value="{{ isset($guest) ? $guest->last_name : '' }}" />
    <input type="text" placeholder="Group" name="group_name"
        class="w-full my-4 rounded-md border border-slate-400 px-4 py-3"
        value="{{ isset($guest) ? $guest->meta['group_name'] : '' }}" />
    <select class="w-full my-4 rounded-md border border-slate-400 px-4 py-3" name="is_couple"
        value="{{ old('is_couple') ?? isset($guest) && $guest->is_couple }}">
        <option @if (old('is_couple') ?? isset($guest) && $guest->is_couple == 1) selected @endif value="1">Billet couple</option>
        <option @if (old('is_couple') ?? isset($guest) && $guest->is_couple == 0) selected @endif value="0">Billet solo</option>
    </select>
    <select class="table_list w-full my-4 rounded-md border border-slate-400 px-4 py-3" style="padding: 10px"
        name="table_id">
        @foreach (\App\Models\Table::all() as $table)
            <option value="{{ $table->id }}" @if (isset($guest) && $guest->table->id === $table->id) selected @endif>{{ $table->name }}
            </option>
        @endforeach
    </select>
</div>
