<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <style>
        @font-face {
            font-family: "Benedict";
            src: url({{ asset('fonts/Benedict.otf') }});
        }

        .benedict {
            font-family: "Benedict";
        }

        .page {
            page-break-after: always;
            page-break-inside: avoid;
            width: 100%;
            height: 100%;
        }

        body {
            background-image: url("https://wilmar.djopa.fr/wp-content/uploads/2023/04/1228A0920-scaled.jpg");
            background-size: cover;
            height: 100vh;
            overflow: auto;
            background-repeat: no-repeat;
        }

        .overlay {
            background: #FFFFFFEE;
            width: 90%;
            height: 90%;
            margin: auto;
        }
    </style>
    @yield('style')
    <script src="https://code.jquery.com/jquery-3.7.0.slim.js"
        integrity="sha256-7GO+jepT9gJe9LB4XFf8snVOjX3iYNb0FHYr5LI1N5c=" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
</head>

<body>
    <div class="container relative h-full mx-auto flex">
        <div class="overlay p-4 mx-auto ">
            @yield('content')
        </div>
    </div>


    @yield('scripts')

</body>

</html>
