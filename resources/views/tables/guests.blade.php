<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
    <link rel="stylesheet" href="//cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
</head>


<body>
    <h1 class="text-3xl font-bold underline text-center my-6">
        Liste des invités par table
    </h1>
    <div class="text-center mb-5">
        @if (isset($request->table))
            <a class="" href="{{ route('table.show') }}">Voir toutes les tables</a>
        @endif
    </div>
    <div class="container">
        <div class="row">
            @foreach ($tables as $table)
                @if ((isset($request->table) && $table->id == $request->table) || !isset($request->table))
                    <div class="col-sm-4 p-2" id="{{ $table->id }}">
                        <div class="border pb-2 rounded-md overflow-hidden">
                            <div class="bg-success text-white p-2 px-3">{{ $table->id }}) {{ $table->name }}</div>
                            @foreach ($table->guests as $guest)
                                <a class="px-3 pt-2 block" href="{{ route('guests.show', $guest) }}">
                                    {{ $guest->first_name }}
                                    {{ $guest->last_name }} ({{ $guest->is_couple ? 'COUPLE' : 'SOLO' }})
                                </a>
                            @endforeach
                        </div>
                    </div>
                @endif
            @endforeach

        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>

    <script src="//cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script>
        let table = new DataTable('#guest');
    </script>
</body>

</html>
