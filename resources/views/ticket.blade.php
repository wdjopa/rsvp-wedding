@extends('simple_layout')
@section('title', "Billet de {{ $guest->first_name }}")
@section('content')
    <form method="POST" action="{{ route('guests.update', $guest) }}"
        class="flex flex-col justify-between text-center justify-center h-full">
        @method('PUT')
        @csrf
        <div>
            <h3 class="text-2xl my-5">
                @if (!isset($guest->meta['status']))
                    Identité de l'invité
                    <input type="hidden" name="status" value="entree" />
                @elseif($guest->meta['status'] == 'entree')
                    L'invité.e est à l'intérieur
                    <input type="hidden" name="status" value="sortie" />
                    ✅
                @elseif($guest->meta['status'] == 'sortie')
                    <input type="hidden" name="status" value="entree" />
                    L'invité.e est à l'extérieur 🚪👈
                @endif
            </h3>

            <div class="font-normal mt-12 text-6xl benedict">
                {{ ucfirst(strtolower($guest->first_name)) }}
                {{ ucfirst(strtolower($guest->last_name)) }}
            </div>
            <div class="font-bold text-2xl my-4 text-green-800">
                <a href="{{ route('table.show', ['table' => $guest->table->id]) }}" target="_blank">
                    {{ $guest->table->name }}
                </a>
            </div>
        </div>
        <div class="mb-10">
            <a class="bg-yellow-400 text-black p-3 px-5 rounded-md" target="_blank"
                href="https://wedding-ticket.wilmar.djopa.fr/single?slug={{ $guest->slug }}&name={{ ucfirst(strtolower($guest->first_name)) . ' ' . ucfirst(strtolower($guest->last_name)) }}&is_couple={{ $guest->is_couple == 0 ? 'false' : 'true' }}">Imprimer
                le billet</a>
        </div>
        <div class="mb-10">
            @if (!isset($guest->meta['status']))
                <button class="bg-green-800 text-white p-3 px-5 rounded-md">
                    Marquer comme entrée
                </button>
            @elseif($guest->meta['status'] == 'entree')
                <button class="bg-red-800 text-white p-3 px-5 rounded-md">
                    Marquer comme sortie
                </button>
            @elseif($guest->meta['status'] == 'sortie')
                <button class="bg-green-800 text-white p-3 px-5 rounded-md">
                    Marquer comme entrée
                </button>
            @endif
        </div>
    </form>
@endsection
