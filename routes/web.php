<?php

use App\Models\Guest;
use App\Models\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/guests", function (Request $request) {
    $guests = Guest::all();
    return view("guests", compact("guests"));
})->name("guests.list");
Route::get("/guests/create", function (Request $request) {
    return view("guests.add_guest");
})->name("guests.create");
Route::post("/guests", function (Request $request) {
    $guest = new Guest();
    $guest->first_name = $request->first_name;
    $guest->last_name = $request->last_name;
    $guest->table_id = $request->table_id;
    $guest->is_couple = $request->is_couple == 0 ? false : true;
    $guest->slug = Str::slug($request->first_name . " " . $request->last_name);
    $meta = [];
    $meta["group_name"] = $request->group_name;
    $guest->meta = $meta;
    $guest->save();
    return redirect()->back()->with("success", "Ajout effectué avec succès");
})->name("guests.store");

Route::delete("/guests/{guest}", function (Request $request, Guest $guest) {
    $guest->delete();
    return redirect()->back()->with("success", "Suppression effectuée avec succès");
})->name("guests.delete");

Route::put("/guests/{guest}", function (Request $request, Guest $guest) {
    if ($request->status) {
        $meta = $guest->meta;
        $meta["status"] = $request->status;
        $guest->meta = $meta;
    } else {
        $guest->first_name = $request->first_name;
        $guest->last_name = $request->last_name;
        $guest->slug = Str::slug($request->first_name . " " . $request->last_name);
        $guest->table_id = $request->table_id;
        $meta = $guest->meta;
        $meta["group_name"] = $request->group_name;
        $guest->is_couple = $request->is_couple == 0 ? false : true;
        $guest->meta = $meta;
    }
    $guest->save();
    return redirect()->route("guests.edit", $guest)->with("success", "Mis à jour avec succès");
})->name("guests.update");

Route::get("/guests/{guest:slug}", function (Request $request, Guest $guest) {
    return view("ticket", compact("guest"));
})->name("guests.show");

Route::get("/guests/{guest}/edit", function (Request $request, Guest $guest) {
    return view("guests.edit", compact("guest"));
})->name("guests.edit");

Route::get("/tables", function (Request $request) {
    $tables = Table::all();
    return view("tables.list", compact("tables"));
});
Route::get("/tables/guests/list", function (Request $request) {
    $tables = Table::all();
    return view("tables.guests", compact("tables", "request"));
})->name("table.show");

Route::get('/registration/tables', function () {
    foreach (file('db/tables.txt') as $line) {
        if (trim($line) !== "") {
            try {
                //code...
                $name = explode(" : ", $line)[0];
                $description = explode(" : ", $line)[1];
            } catch (\Throwable $th) {
                Log::debug($line);
                Log::debug($th->getMessage());
            }
            try {
                //code...
                $table = Table::where("name", $name)->firstOrFail();
            } catch (\Throwable $th) {
                $table = new Table();

                //throw $th;
            }
            $table->name = $name;
            $table->description = $description;
            $table->save();
            // loop with $line for each line of yourfile.txt
        }
    }
    dd(Table::count() . " tables registered");
});

Route::get('/registration/guests', function (Request $request) {
    $totalBefore = Guest::count();
    $tableId = 0;
    $isCouple = false;
    $tableCouple = false;
    Log::debug("");
    Log::debug("");
    Log::debug("============================");
    Log::debug("INVITES $request->group_name");
    Log::debug("============================");
    Log::debug("");
    $errors = [];

    foreach (file('db/guests_' . $request->group_name . '.txt') as $line) {
        try {
            if (trim($line) !== "" && $line !== "\n") {
                if (str_contains($line, "TABLE")) {
                    $tableId = explode(" ", $line)[1];
                    $couple = sizeof(explode(" : ", $line)) >= 2 ? explode(" : ", $line)[1] : "";
                    if (strtoupper(trim($couple)) === "COUPLES") {
                        $tableCouple = true;
                    }
                    Log::debug("+++++++++++");
                    Log::debug("Table ID $tableId");
                    Log::debug("");
                    Log::debug("");
                } else {
                    $items = explode("+", $line);
                    if (sizeof($items) > 1)
                        $isCouple = false;
                    else
                        $isCouple = $tableCouple;

                    if (str_contains(strtolower($line), "couple")) {
                        $isCouple = true;
                    }

                    foreach ($items as $item) {
                        $item = trim($item);
                        $firstName = sizeof(explode(" ", $item)) >= 2 ? explode(" ", $item)[1] : ".";
                        $lastName = explode(" ", $item)[0];
                        try {
                            $guest = Guest::where("first_name", $firstName)->where("last_name", $lastName)->firstOrFail();
                        } catch (\Throwable $th) {
                            $guest = new Guest();
                        }

                        $guest->first_name = ($firstName);
                        $guest->last_name = ($lastName);
                        $guest->slug = Str::slug($firstName . " " . $lastName);
                        $guest->table_id = $tableId;
                        $guest->is_couple = $isCouple ?? $request->is_couple ?? false;
                        $meta = [];
                        $meta["group_name"] = $request->group_name;
                        $guest->meta = $meta;
                        $guest->save();
                        Log::debug("$guest->first_name $guest->last_name (" . ($guest->is_couple ? "COUPLE" : "SOLO") . ")");
                    }
                }
            }
        } catch (\Throwable $th) {
            $errors[] = $line;
            \Log::debug("Something went wrong on guest creation. Error: " . $th->getMessage());
        }
    }

    $totalAfter = Guest::count();

    dd($totalAfter - $totalBefore . " guests registered now ($totalAfter in total)", $errors);

    // return view('welcome');
});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
